---
layout: markdown_page
title: "Category Direction - Runner Fleet"
description: " Features and capabilities for installing, configuring, managing and monitoring a fleet of GitLab Runners."
canonical_path: "/direction/verify/runner_fleet/"
---

- TOC
  {:toc}

## Runner Fleet

Our vision is to provide users with a birds-eye view, configuration management capabilities, and predictive analytics to easily administer a fleet of GitLab Runners.  This category's north star delivers an industry-leading user experience as measured by system usability scores and the lowest maintenance overhead for customers who self-manage a CI build platform at scale. Features in this category will primarily span the Admin and Group views in the GitLab UI.

## Who we are focusing on?

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Runner, our "What's Next & Why" are targeting the following personas, as ranked by priority for support:

1. [Priyanka - Platform Engineer](/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
2. [Devon - DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
3. [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
4. [Delaney - Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)

## Strategic Priorities

The table below represents the current strategic priorities for Runner Fleet. This list will change with each monthly revision of this direction page.

|Item|Why?| Target delivery QTR |
|----------|----------------|---------------------|
|[Runner Admin View Usability Improvements](https://gitlab.com/groups/gitlab-org/-/epics/5665)|The 2021 Q3 UX scorecard of the current runner administrative user experience identified several areas that needed improvement regarding workflow and user experience. Throughout FY23Q1, we have iteratively redesigned and added new features to the Admin Area - Runners view. The next iteration will add the ability to [bulk delete](https://gitlab.com/gitlab-org/gitlab/-/issues/339525) runners from this view.| FY23 Q2             |
|[Runner Group View Usability Improvements](https://gitlab.com/groups/gitlab-org/-/epics/6714)|The usability improvements described above for the admin view are relevant to the group view. Customers on GitLab SaaS use this interface to administer runners they self-manage. Therefore, any usability and feature improvements in the Admin view must also be made available in the Group view. In 14.10, we shipped the new Group View, which delivered the same usability and feature enhancements added to the Admin Area Runners view. We also plan to add the [bulk delete](https://gitlab.com/gitlab-org/gitlab/-/issues/361721) feature to this view to finalize the usability improvements.| FY23 Q2             |
|[Runner Search, Association and Ownership for Admin and Group views](https://gitlab.com/groups/gitlab-org/-/epics/7181)|Including an assigned group or project data element and adding the ability to export the runner data from GitLab will significantly simplify the operational overhead of locating and troubleshooting runners across an enterprise installation of GitLab.| FY23 Q2             ||
|[Runner Admin View New Features](https://gitlab.com/groups/gitlab-org/-/epics/6716)|With the foundations in place, we will now be able to deliver new features more quickly. These include summary metrics on all runners associated at the instance, group, or project levels, improved search and filtering, runner association to a project or group, automated removal of stale runners.| FY23 Q2           |
|[Runner Group View New Features](https://gitlab.com/groups/gitlab-org/-/epics/5677)|With the foundations in place, we will now be able to deliver new features more quickly. These include improved search and filtering, runner association to a project or group, automated removal of stale runners.| FY23 Q3 |
|[Runner Fleet - Runner queue wait times](https://gitlab.com/groups/gitlab-org/-/epics/5667)| A critical question for our customers who use GitLab SaaS or those managing GitLab at scale is the estimated wait time for a job to start on a Runner. This problem and the related use cases are the ones that we plan to address as we evolve the Runner Fleet [vision](https://gitlab.com/gitlab-org/gitlab/-/issues/345594/) and implement new features associated with Runner queue monitoring and fleet performance.|FY23 Q4|   

## Maturity Plan

- Runner Fleet is at the ["Viable"](/direction/maturity/) maturity level.
- As detailed in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/6090), we plan to review the maturity scorecard for runner core and complete new category maturity scorecards for the other product development categories, Runner SaaS, and Runner Fleet.

## Competitive Landscape

At GitLab, a critical challenge that we are trying to solve is simplifying the administration and management of a CI/CD build fleet at an enterprise scale. We notice that other competitors are also investing in this general category. Github recently [announced](https://github.blog/2022-02-23-new-way-understand-github-hosted-runner-capacity/) a new management experience that provides a summary view of GitHub-hosted runners. This is a signal that there will be a focus on reducing maintenance and configuration overhead for managing a CI/CD build environment at scale across the industry.

## Give Feedback

The near features highlighted here represent just a subset of the features and capabilities that have been requested by the community and customers. If you have questions about a specific runner feature request or have a requirement that's not yet in our backlog, you can provide feedback or open an issue in the GitLab Runner [repository](https://gitlab.com/gitlab-org/gitlab-runner/-/issues).

## Revision Date

This direction page was revised on: 2022-05-07