---
layout: handbook-page-toc
title: Commissions
---



## **Charter**

Field Commissions is a part of Field Operations, reporting into Sales Operations. Our goal is to incentivize the field to generate Net ARR, collaborate with other teams to make GitLab successful and process commissions in an accurate and timely fashion.  We aim to be viewed as a trusted business partner for Field Sales, Marketing, Finance, Accounting and GTM Strategy.

## **Meet The Team**

- Lisa Puzar, Senior Manager, Sales Commissions
- Swetha Kashyap, Sales Commissions Manager
- Praveen Ravi, Sales Commissions Analyst
- Sophie Hamann, Sales Commissions Analyst

**The [Sales Commissions Handbook](https://docs.google.com/document/d/1XE8fXmJBTibVuuKCju3MsXPZvE2qWHu-_kBuMPsGFDE/edit?usp=sharing) is in the process of being moved to our GitLab internal handbook.**
