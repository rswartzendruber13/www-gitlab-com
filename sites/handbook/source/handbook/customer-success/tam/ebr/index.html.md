---
layout: handbook-page-toc
title: "Executive Business Reviews (EBRs)"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## What is an Executive Business Review?

An Executive Business Review (EBR) is a strategic meeting with stakeholders from both GitLab and the customer. It is an extension of the account planning process and part of the TAM's regular business rhythm. In partnership with the SAL/ AE, a TAM is responsible for working with their customers to identify the primary objectives and desired business outcomes for the meeting, then scheduling and conducting the EBR. In addition, it is recommended to work with the champion to ensure the right content is covered for the audience. The TAM will own the content creation, and together with the SAL/AE, the TAM will determine the flow of the review based on best practices and insight into the customer relationship.

TAMs should hold EBRs with each of their customers **at least** once per year. Larger ARR accounts or those with more strategic initiatives may benefit from or even require a biannual or quarterly cadence, a decision to be made by the TAM, SAL/AE, and Sales/CS leadership.

The EBR aims to demonstrate to the [Economic Buyer](/handbook/sales/#capturing-meddpicc-questions-for-deeper-qualification) the value they are getting in their partnership with GitLab. It is interactive from both sides, discussing the customer's desired business outcomes and related metrics, progress against these metrics and desired outcomes, and aligning on strategic next steps.  The most crucial element in all EBRs is giving the buyer the time to speak about what matters to them, and creating a framework to enable them to do so.

### EBR Enablement

- A SAL/AE can be a great partner in positioining EBRs. For more on the value of EBRs, with customer examples, please watch [this internal EBR enablement session](https://www.youtube.com/watch?v=1CL5YQeC8Fs).
    - [Deck](https://docs.google.com/presentation/d/1sWj5HJR4IxdOMg7GiuEjlS5GvFYzYdtLW3QsX6IsxCM/edit#slide=id.g20a53d489f_0_213) used in the recording (internal)
    - [Objection handing document](https://docs.google.com/document/d/1WzvklnloC22WffVcLYupLy3kqSsEbHNtcj47xrb07As/) referred to in the recording (internal)
- Follow [this link](https://gitlab.edcast.com/pathways/tam-executive-business-reviews) to access the EBR certification on EdCast.
- [Chorus Playlist](https://chorus.ai/playlists/741604) gives you access to the recordings of the best TAM calls. The recordings are continuously updated, so keep this playlist bookmarked for best practices.

## How to bring up an EBR with your customers

To schedule the EBR, the [account team meeting](/handbook/customer-success/account-team/#account-team-meeting) is the best time to review customers that are due an EBR, and agree on next steps, ensuring the economic buyer and key decision makers are invited and able to attend where possible.

During a cadence call, the TAM should bring up the topic of an EBR. If this is the customer's first EBR with GitLab, explain to them what it is and what value it brings. Ensure they know key stakeholders should attend and ask them to start revewing calendars for an ideal time to schedule the EBR. After the cadence call, follow up with a [written summary of the EBR](#sample-ebr-emails) and reiterate your ask for help in scheduling. It can take up to 3 months to find a good time for stakeholders to all be available to meet with us, so the earlier we can start the conversation the better.

Please also view our [EBR Playbook](https://docs.google.com/spreadsheets/d/1nGjXMaeAFWEOGdsm2DPW-yZEIelG4sy46pX9PbX4a78/edit#gid=0) (internal to GitLab) for more details on how to propose, prepare, and present an EBR. This internal playbook also includes a link to [EBR sell sheets](https://drive.google.com/drive/folders/1MYIIEqOZ_lskuVUt4S-lkz1HR79ZbDjj?usp=sharing), which TAMs can copy and edit to send to their customers to help demonstrate what the customer will get out of the EBR, as well as an "[EBR in a Box](https://docs.google.com/presentation/d/1V3wzIZ9j6pVUbXpSeJgA_Lk-97C7_vr8NUJWk4J0__s/edit?usp=sharing)" presentation which contains several pointers on the logistics of preparing, such as a suggested timeline, how to prepare and tips on presenting.

### Scheduling and Attendee Challenges

Our goal with an Executive Business Review is to meet with a customer's senior leadership and decision makers to review their business objectives and priorities for their digital transformation. We should work with our main points of contact to schedule those personas to attend the EBR. However, if after two attempts to request an EBR with the decision makers and influencers the TAM is still unsuccessful, extend an upcoming cadence call to one hour and use this sesion to show the value of the EBR conversation. 

This customer push-back is most common when delivering our first EBR to a customer. The important thing in this situation is to deliver an EBR to demonstrate the value of the meeting, and have a targeted conversation about the customer's objectives. This should help scheduling future EBRs with a customer's leadership.

#### EBR Length

The ideal length for EBRs is 90 minutes, but this is not always realistic. For example, customers may not be able to commit to that much time, or it may be best for the TAM to have an EBR be shorter, both to ensure it is impactful and valuable but also to not overburden them.

Please see below for some tips on how to shorten EBRs to 60, or even 30, minutes:

- Highlight recent successes of your primary team and champions upfront in the call (e.g. during introductions), just in case you run out of time at the end.
- No matter the length of the EBR, the business objectives & goals portion should be at least 75% of the call and should begin as early in the conversation as possible, as this is the reason we're here.
  - To help break it up even further, consider 50% of the call to be focused on existing objectives (validation, review of progress, how GitLab will help achieve those goals, questions from both sides on those objectives, etc.), and 25% to be open-ended conversation on future or additional objectives not already discussed. Depending on your familiarity of the account, those percentages may be swapped, as you'll focus more time on learning about their objectives than discussing what you already know.
- Avoid tactical conversation topics such as feature requests, support tickets, etc., as these can be discussed during cadence calls and are not strategic or relevant for the Economic Buyer.
- Be very selective of if you want to have a product update or overview. It's recommended to use the EBR to get buy-in on further product adoption that are most aligned with the customer's needs and using the opportunity to secure agreement on a follow up session. However, if a product overview during the EBR would help drive buy-in and ROI, ensure it is extremely relevant, succinct, and tied directly to their business objectives.
- As always, prepare your next steps to be both actionable and accountable, and ensure that any next steps that formed throughout the EBR itself are captured and shared with the customer afterwards.
- If you only have 30 minutes, forgo the slides and presentation, but still come prepared with everything that would have been in the slides documented in a clear format that can be shared with the customer afterwards.

### Sample EBR Request Talk-Track

*As we are halfway through our current year of partnership, I wanted to schedule some time for us to meet and discuss progress against your business objectives and key initiatives. The goal of this time together is to:*

1. *Amplify your accomplishments*
1. *Show the progress against your key initiatives that you and the team have made*
1. *Ensure we are aligned on (company name)'s upcoming key business initiatives*

*We'd like to invite xxx (influencers, decision-makers) to join also, and we'll have some of our leadership team join.  Would you suggest that my leadership or I reach out to them directly, or what is the best way to get time on everyone's calendar?*

### Sample EBR Emails

#### Initiated by the TAM

The following is an example for a TAM sending the email if not broached on a call. Some language would need to be modified for the specifics of the account (e.g., collect or refine objectives, goals of the meeting, the expected time for EBR). This is intended to be an example, not a cut and paste.

*Hello (Name)*

*I am reaching out to determine the best date and time for our bi-annual strategic review. As a reminder on the purpose of these meetings, we look to cover the following:*

1. *Review our combined progress on delivering to your goals and business outcomes*

2. *Collect and discuss future goals and desired success measures*

3. *Discuss product use cases of interest and/or future roadmap items*

4. *Provide an update on our delivery to your needs (e.g., delivery to your enhancement requests, support cases and SLA)*

*We'd like to invite xxx (influencers, decision makers) to join, and we'll have some of our leadership team join as well.  Would you suggest that I or my leadership reach out to them directly, or what is the best way to get time on everyone's calendar?*

*The following are some suggested dates and times - typically these reviews take 60-90 minutes. I'm looking forward to review future strategy with you and the team!*

##### Email templates by region

These can be used as a starting point to send an EBR proposal to customers, and for ease of use and fast updating, they are available in Google Docs:

1. [DACH/Germany](https://docs.google.com/document/d/1b_c_UYrZBFyegDZN_fmYcMraQ9NHdrsIndSkPMp0X_Y/edit) (GitLab internal)

#### Initiated by GitLab leadership

The following is an example for when a GitLab senior leader or executive sends the email. Some language would need to be modified for the specifics of the account (e.g., collect or refine objectives, goals of the meeting, the expected time for EBR). This is intended to be an example, not a cut and paste.

*Introducing myself, I'm the (insert role) at Gitlab and would like to personally thank you for your business and trust in Gitlab.*

*(Insert TAM name), your TAM, has shared with me several updates regarding our amazing partnership and the rapid adoption of GitLab at (Customer Name).  I would like to personally invite you to an Executive Business Review (EBR) that is a key part of our engagement and collaboration. Some key objectives include:*

1. *Review our combined progress on delivering to your goals and business outcomes, holding us to account to deliver expected value, and celebrating achievements and successes*
1. *Collect and discuss future goals and desired success measures*
1. *Discuss product use cases of interest and/or future roadmap items*
1. *Provide an update on our delivery to your needs (e.g., delivery to your enhancement requests, support cases and SLA)*

*Ultimately, we seek to ensure we're aligned on the path forward to continue delivering to your business needs with the GitLab platform. If you're open to it, we will coordinate with you to find a time that best suits your availability. The EBRs typically take 60-90 minutes.*

*Please let me know if you have any questions.*

## EBRs in Gainsight

A [CTA in Gainsight](/handbook/customer-success/tam/gainsight/#ctas) that will automatically open seven months before renewal, with a due date of 45 days later to give time to schedule, prepare for and conduct the EBR. If doing a more frequent business review, please manually open a CTA, and within this CTA, open the "EBR" playbook. The CTA is where you will track the completion of tasks necessary for a successful EBR.

If an account is newly TAM-qualifying and you receive a CTA to hold an EBR, use the snooze CTA functionality to push out the date to when you think will be good timing for an EBR.

If no progress has been made on the CTA (no tasks have been completed) and there are less than 2 months before renewal, the CTA will be closed automatically. Alternatively, if you do not plan to hold the EBR within the next quarter, please close the CTA, chosing the appropriate close reason and including additional context in the CTA details.

## How to prepare an EBR

### Content

EBRs typically consists of the following content:

1. Introductions to key team members/those present for the EBR
    - This should be a brief roundtable for everyone to say their name & role for easy recognition throughout the call
1. Overview/Update on GitLab
    - This is often especially valuable for the Economic Buyer in order to understand the full capabilities of GitLab
1. Reiteration of the customer's goals and desired positive business outcomes as we understand them
    - Address each of the customer's objectives in their success plan, including the status, work left to do, and how we'll measure success
    - Prepare several probing impact questions related to each objective to get the conversation going
1. Review of progress made against these goals and desired outcomes
    - Direct reference to any desired outcomes/agreed goals from previous EBRs and updates against these specifically
1. Aligning with the key decision makers on their strategic vision
    - What goals do they have that we did not discuss? Are any of the goals we discussed not a priority for them?
1. Recommendations & Next Steps

Also to be considered:

1. Product Roadmap (focus on 1-2 specific categories important to them and loop in the Product team accordingly)
1. Year in Review (more usage focused, see below)
1. Support Review (how many tickets at what priority, SLAs, etc.)
1. [Delivered Enhancements](/handbook/customer-success/tam/issue-prioritization/#executive-business-reviews) (highlight released feature requests that were important to them, velocity of releasing feature requests, etc.)

[Usage ping](https://docs.gitlab.com/ee/development/usage_ping/) can provide data to build the usage and growth story. If usage ping is not enabled, a [payload](https://docs.gitlab.com/ee/development/usage_ping/#usage-ping-payload) may be periodically requested from the customer. Extracting the payload is a simple process and only takes about 30 seconds. Some examples included in this data are historical and current user count, and CI build history. Please note usage ping only goes back 6 months, so it's worthwhile to keep old data for comparison in future EBRs.

### Before the call

Besides creating the deck, there are a few other items to consider to ensure your EBR will go as smoothly as possible.

1. It is highly encouraged to review and even build your deck with your champions to protect you and the customer from surprises. This co-creation process builds trust and creates a much more impactful EBR through collaboration. The collaboration enables the champion to add their major topics and contribute to the goals-based content as they will always be much closer to their objectives and outcomes.
1. Create a dedicated SLACK thread in the specific internal customer channel with the GitLab team members attending 1hr before the EBR, to share the deck, call notes draft and remind them that taking notes is critical to capture the customer input / goals / key topics. 
1. Ask someone else who will be on the call (e.g. your manager) to take notes and create a Google doc for notes in the customer folder. Having a dedicated notetaker ensures that you're able to effectively lead the conversation as well as focus on your content instead of switching between windows.
1. There are usually specific product areas that customers have special interest in, and having the Product Manager for that area on the call can be incredibly valuable. Once you've scheduled the EBR and determined which PM would be most appropriate, reach out to the PM to ask if they are available and willing to join the presentation with an overview of recently delivered product functionality, as well as what features will be coming within the next year. Also set expectations for the time commitment expected from them; for example, 15 or 30 minutes at the end of the EBR. Once you've confirmed their availability, add them to the calendar invite and share your deck with them and add a dedicated section of the deck for them to add slides to. Make sure you follow up 48 hours in advance of the EBR if they haven't added their content.
1. If you want a GitLab VP to participate in the EBR, follow the instructions on [VP scheduling](https://about.gitlab.com/handbook/eba/vp-scheduling/).

### Impact Questions

This is a list of example impact questions the TAM can use throughout the EBR to dig deeper into the customers objectives and strategy.

1. Tell us about your largest initiatives right now? Where are you focused?
1. Can you spend some time on your overall cloud and app modernization strategy?
1. As you look at the year ahead, what are your key 2-3 initiatives?
  1. What has changed since last year?
  1. What would you like things to look like in the next couple years - where do you want to be?
1. On the security front, we see customers struggling with the triage process. When you have a security issue, what does the resolution process look like? Who is involved? 
1. How do security vulnerabilities affect your cycle time?
1. What visibility or insights does your leadership have? How are you collecting this information today? 
1. (If customer describes an objective with no metrics) Would you please describe what 'good' will look like for this initiative?  How are you thinking about/measuring progress against this effort? 



### EBR Examples & Template

To create the content for the EBR, please review the [EBR Template](https://docs.google.com/presentation/d/1Ubfsa7GgU7XEtr0IuPDaDA9s5dkm64u26T0Me08WP68/edit).  Note this template is aimed at EBRs 60-90 minutes in length, and the account team may want to consider adding or removing elements depending on most relevant content and time allotted. Please watch the [EBR interview](https://www.youtube.com/watch?v=luxIdKtGB_s&ab_channel=GitLabUnfiltered) for a walkthrough of the three major components of an EBR and tips & tricks on successful delivery!

There are also several examples EBR decks and past recordings linked in the [Chorus Playlist](https://chorus.ai/playlists/741604) (Internal Only Link) and [EBR in a Box](https://docs.google.com/presentation/d/1V3wzIZ9j6pVUbXpSeJgA_Lk-97C7_vr8NUJWk4J0__s/edit?usp=sharing) (Internal Only Link) for TAMs and other GitLab team members to review and take inspiration from (please keep all customer-specific content confidential).

### EBR Tips / Learnings 

The following tips / learnings should further help TAMs when planning, executing EBRs with their customers.

1. Try to plan multiple EBRs in a short timeframe like 6 in a single quarter. This constant focus on the EBRs will help you to:
     - Grow your confidence in preparing and most of all executing EBRs.
     - Build your perfect EBR templates (deck, emails, issue in collaboration project), specifically for your region / language.
     - Find your personal messaging & flow within the deck/slides, which makes the EBR feel even more personal/partnership like for the customer.
1. When moderating and executing an EBR, simply keep the focus on forward looking discussions (90%), (10% retrospective). 
     - Let the customer and their team speak as much as possible. This is driving the discussions which you want to have in an EBR.
1. After the EBR is over, immediately try to set up a quick 10min Zoom call with the GitLab Team members who joined the EBR to:
     - Ask for feedback on what went well and what can be improved in next EBR. Document this in the notes for later reference.
     - Align on the key topics, which have been communicated / discussed with the customer. 
1. Record the EBR via Zoom. Make sure you ask the audience / customer for their permission before starting to record. 
     - In your gdoc notes document, add a marker / agenda point at the top, to ask for permission to record. 
     - Having all of the EBRs recorded as video, makes it a breeze to review yourself in a quiet part of the day and further iterate on things which you can do better the next time.
1. Communicate the ask for an EBR as early as possible. 
     - Expect that the date will slip by several weeks, as management attendance is hard to coordinate.
1. Even when you prepared the EBR and scheduled it:
     - Expect that it gets cancelled again. This is normal, and all your hard work was still worth it. You will be able to leverage the learnings sooner than you think
 




